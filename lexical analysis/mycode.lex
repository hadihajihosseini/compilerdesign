%{
    #include <stdio.h>
    #include <string.h>
    #include <regex.h> 
    void printInFile(char*);
	void printTokenName(char*, char*);
    FILE* myDestFile;
	int numberOfCurrentLine = 1;
%}

%option noyywrap

deliminator     [\t\n ]
whitespace		{deliminator}+
letter          [a-zA-Z]
digit           [0-9]
id              ({letter})({letter}|{digit}|_)*
integer			{digit}+
float			{digit}*\.{digit}+(E(\+|-){digit}+)?

%%

void		{
				printTokenName("TOKEN_VOIDTYPE", yytext);
			}

int			{
				printTokenName("TOKEN_INTTYPE", yytext);
			}

foreach		{
				printTokenName("TOKEN_LOOP", yytext);
			}

return		{
				printTokenName("TOKEN_RETURN", yytext);
			}

if			{
				printTokenName("TOKEN_IFCONDITION", yytext);
			}

double		{
				printTokenName("TOKEN_DOUBLETYPE", yytext);
			}

("+"|"-"|"*"|"^"|"/")		{
			printTokenName("TOKEN_ARITHMATICOP", yytext);
		}

("&&"|"||"|\!) {
			printTokenName("TOKEN_LOGICOP", yytext);
		}

("&"|"|")	{
			printTokenName("TOKEN_BITWISEOP", yytext);
		}

("<="|"<"|">"|">="|"=="|"!=")	{
			printTokenName("TOKEN_RELATIONOP", yytext);
		}

[=]	{
			printTokenName("TOKEN_ASSIGNOP", yytext);
		}

"("		{
			printTokenName("TOKEN_LEFTPAREN", yytext);
		}

\)		{
			printTokenName("TOKEN_RIGHTPAREN", yytext);
		}

\{		{
			printTokenName("TOKEN_LCB", yytext);
		}

\}		{
			printTokenName("TOKEN_RCB", yytext);
		}

;		{
			printTokenName("TOKEN_SEMICOLON", yytext);
		}

,		{
			printTokenName("TOKEN_COMMA", yytext);
		}

\.\.	{
			printTokenName("TOKEN_UNTIL", yytext);
		}

\[		{
			printTokenName("TOKEN_LB", yytext);
		}

\]		{
			printTokenName("TOKEN_RB", yytext);
		}

" "+	{
			printTokenName("TOKEN_WHITESPACE", "[space]");
		}

\n		{
			numberOfCurrentLine++;
			printTokenName("TOKEN_WHITESPACE", "\\n");
		}

\t		{
			printTokenName("TOKEN_WHITESPACE", "\\t");
		}

[/][*][^*]*[*]+([^*/][^*]*[*]+)*[/]	{
			printTokenName("TOKEN_COMMENT", yytext);

			// counts number of occurance of \n and then adds
			// the number to the numberOfCurrentLine variable
			// to keep the number of lines
			char* str = yytext;
			int i = 0;
        	char *pch=strchr(str,'\n');
			while (pch!=NULL) {
				i++;
				pch=strchr(pch+1,'\n');
			}
			numberOfCurrentLine+=i;
		}

[/][*] 	{
			fprintf(myDestFile, "error in line %d : multiline comment opened but never closed\n", numberOfCurrentLine);
			return 1;
		}

[^\n\t;]*[*][/]	{
			fprintf(myDestFile, "error in line %d : unopened comment section\n", numberOfCurrentLine);
			return 1;
		}

"//".*	{
			printTokenName("TOKEN_COMMENT", yytext);
}

float	{
			printTokenName("TOKEN_FLOATTYPE", yytext);
		}

char	{
			printTokenName("TOKEN_CHARTYPE", yytext);
		}

string	{
			printTokenName("TOKEN_STRINGTYPE", yytext);
		}

main	{
			printTokenName("TOKEN_MAINFUNC", yytext);
		}

break	{
			printTokenName("TOKEN_BREAKSTMT", yytext);
		}

continue	{
			printTokenName("TOKEN_CONTINUESTMT", yytext);
		}

("-"|"+")?{integer}	{
			char* lastStartChar = yytext;
			int isPositive = 1;
			int error = 0;
			int ommited = 0;
			if(yytext[0] == '-'){
				yytext = &yytext[1];
				ommited = 1;
				isPositive = 0;
				if(strlen(yytext)>strlen("2147483648")){
					error = 1;
				}else if(strlen(yytext)==strlen("2147483648")){
					if(strcmp(yytext, "2147483648")>0){
						error = 1;
					}
				}
			}
			else if(yytext[0] == '+'){
				yytext = &yytext[1];
				ommited = 1;
				if(strlen(yytext)>strlen("2147483647")){
					error = 1;
				}else if(strlen(yytext)==strlen("2147483647")){
					if(strcmp(yytext, "2147483647")>0){
						error = 1;
					}
				}
			} else {
				if(strlen(yytext)>strlen("2147483647")){
					error = 1;
				}else if(strlen(yytext)==strlen("2147483647")){
					if(strcmp(yytext, "2147483647")>0){
						error = 1;
					}
				}
			}
			if(error == 1){
				fprintf(myDestFile, "error in line %d : integer constant is out of range\n", numberOfCurrentLine);
				return 1;
			}else{
				if(ommited == 1) yytext = lastStartChar;
				printTokenName("TOKEN_INTCONST", yytext);
			}
		}

{float}	{
			printTokenName("TOKEN_FLOATCONST", yytext);
		}

["][^["]]*["]	{
			printTokenName("TOKEN_STRINGCONST", yytext);
		}

"'"{letter}"'"	{
			printTokenName("TOKEN_CHARCONST", yytext);
		}

{id}	{
			// if((yytext[0]>='a' && yytext[0]<='z')
			// 	|| (yytext[0]>='A' && yytext[0]<='Z')){
			if(strlen(yytext) > 31) {
				fprintf(myDestFile, "error in line %d : id length can't be more that 31 letters\n", numberOfCurrentLine);
			}
			printTokenName("TOKEN_ID", yytext);
			// 	}
			// else{
			// 	fprintf(myDestFile, "error in line %d : wrong id definition\n", numberOfCurrentLine);
			// }
		}

({digit}|"_")+({id}|"_"|{digit})+ {
				fprintf(myDestFile, "error in line %d : wrong id definition\n", numberOfCurrentLine);
				return 1;
		}

.		{
			printTokenName("didn't catch", yytext);
		}
%%

int main() {
    /*
        MohammadHadi HajiHosseini 9626183
    */
    char filePath[1000];
	// printf("Enter the address of the code file:\n");
    // scanf("%s", filePath);
	// FILE* mySourceFile = fopen(filePath, "r");

	FILE* mySourceFile = fopen("samplecode.txt", "r");

	myDestFile = fopen("tokenized.txt", "w");

	yyin = mySourceFile;
	yylex();
}

void printInFile(char* text) {
    fprintf(myDestFile, "%s", text);
}

void printTokenName(char* tokenName, char* token){
	printInFile(tokenName);
	for(int i = 0; i<30-(int)strlen(tokenName); i++){
		printInFile(" ");
	}
	printInFile(token);
	printInFile("\n");
}
