%{
    #include <iostream>
    using namespace std;
    extern int yylex();
    // extern FILE* yyin;
    int yyerror(const char* error);
%}

%union {
    int intVal;
    char charVal;
    char* string;
}

%token <charVal> COMMA EXCLAMATION_MARK FULL_STOP;
%token <intVal> NUM;
%token <string> ID;
%token SPACE NEW_LINE TAB;

%%

program:            sentence                                   {cout << "All done" << endl;}
                    ;

sentence:           word whitespace sentence                
                    | word sign whitespace sentence
                    |
                    ;

word:               NUM | ID;

sign:               EXCLAMATION_MARK | COMMA | FULL_STOP;

whitespace:         SPACE whitespace
                    | TAB whitespace
                    | NEW_LINE whitespace
                    | SPACE
                    | NEW_LINE
                    | TAB;

%%

int main(int argc, char* argv[]) {
    // yyin = fopen(argv[1], "r");
    yyparse();
    return 0;
}

int yyerror(const char* error){
    cout << "There is one error: " << error << endl;
    return 0;
}