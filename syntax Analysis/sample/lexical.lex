%{
    #include <iostream>
    #include "./myapp_bison.hpp"
    using namespace std;
%}

%option noyywrap

%%

[.]         {
    return FULL_STOP;
}

[!]         {
    return EXCLAMATION_MARK;
}

[,]         {
    return COMMA;
}

[a-zA-Z]+   {
    return ID;
}

[0-9]+      {
    return NUM;
}

" "         {
    return SPACE;
}

"\n"        {
    return NEW_LINE;
}

"\t"        {
    return TAB;
}

%%

// int main() {
//     /*
//         MohammadHadi HajiHosseini 9626183
//     */
    
//     // char filePath[1000];

// 	// printf("Enter the address of the code file:\n");
//     // scanf("%s", filePath);
// 	// FILE* mySourceFile = fopen(filePath, "r");

// 	// FILE* mySourceFile = fopen("samplecode.txt", "r");

// 	// myDestFile = fopen("tokenized.txt", "w");

// 	// yyin = mySourceFile;
// 	yylex();
// }
