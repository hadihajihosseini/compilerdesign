%{
    #include <stdio.h>
    #include <iostream>
    #include "./myapp_bison.hpp"
    #include <string.h>
    #include <regex.h> 
    using namespace std;

    void printInFile(char*);
	void printTokenName(char*, char*);
    FILE* myDestFile;
	int numberOfCurrentLine = 1;
%}

%option noyywrap

deliminator     [\t\n ]
whitespace		{deliminator}+
letter          [a-zA-Z]
digit           [0-9]
id              ({letter})({letter}|{digit}|_)*
integer			{digit}+
float			{digit}*\.{digit}+(E(\+|-){digit}+)?

%%

void		{
				return TOKEN_VOIDTYPE;
			}

int			{
				return TOKEN_INTTYPE;
			}

foreach		{
				return TOKEN_LOOP;
			}

return		{
				return TOKEN_RETURN;
			}

if			{
				return TOKEN_IFCONDITION;
			}

else        {
                return ‫‪TOKEN_ELSECONDITION‬‬;
            }

print       {
                return ‫‪TOKEN_PRFUNC‬‬;
            }

double		{
				return TOKEN_DOUBLETYPE;
			}

// ("+"|"-"|"*"|"^"|"/")		{
// 			return TOKEN_ARITHMATICOP;
// 		}
"+"			{
				return TOKEN_ARITHMATICOP_SUM;
			}
"-"			{
				return TOKEN_ARITHMATICOP_MINUS;
			}
"*"			{
				return TOKEN_ARITHMATICOP_MULTIPLY;
			}
"^"			{
				return TOKEN_ARITHMATICOP_POWER;
			}
"/"			{
				return TOKEN_ARITHMATICOP_DIVIDE;
			}

// ("&&"|"||"|\!) {
// 			return TOKEN_LOGICOP;
// 		}
"&&"		{
				return TOKEN_LOGICOP_AND;
			}
"||"		{
				return TOKEN_LOGICOP_OR;
			}
"!"			{
				return TOKEN_LOGICOP_NOT;
			}

// ("&"|"|")	{
// 			return TOKEN_BITWISEOP;
// 		}
"&"			{
				return TOKEN_BITWISEOP_BITAND;
			}
"|"			{
				return TOKEN_BITWISEOP_BITOR;
			}

// ("<="|"<"|">"|">="|"=="|"!=")	{
// 			return TOKEN_RELATIONOP;
// 		}
[<][=]		{
				return TOKEN_RELATIONOP_LESSTHANOREQUAL;
			}
[<]			{
				return TOKEN_RELATIONOP_LESSTHAN;
			}
[>]			{
				return TOKEN_RELATIONOP_MORETHAN;
			}
[>][=]		{
				return TOKEN_RELATIONOP_MORETHANOREQUAL;
			}
[=][=]		{
				return TOKEN_RELATIONOP_EQUAL;
			}
[!][=]		{
				return TOKEN_RELATIONOP_NOTEQUAL;
			}

[=]	{
			return TOKEN_ASSIGNOP;
		}

"("		{
			return TOKEN_LEFTPAREN;
		}

\)		{
			return TOKEN_RIGHTPAREN;
		}

\{		{
			return TOKEN_LCB;
		}

\}		{
			return TOKEN_RCB;
		}

;		{
			return TOKEN_SEMICOLON;
		}

,		{
			return TOKEN_COMMA;
		}

\.\.	{
			return TOKEN_UNTIL;
		}

\[		{
			return TOKEN_LB;
		}

\]		{
			return TOKEN_RB;
		}

" "+	{
			return TOKEN_WHITESPACE;
		}

\n		{
			numberOfCurrentLine++;
			return TOKEN_WHITESPACE;
		}

\t		{
			return TOKEN_WHITESPACE;
		}

[/][*][^*]*[*]+([^*/][^*]*[*]+)*[/]	{
			return TOKEN_COMMENT;

			// counts number of occurance of \n and then adds
			// the number to the numberOfCurrentLine variable
			// to keep the number of lines
			char* str = yytext;
			int i = 0;
        	char *pch=strchr(str,'\n');
			while (pch!=NULL) {
				i++;
				pch=strchr(pch+1,'\n');
			}
			numberOfCurrentLine+=i;
		}

[/][*] 	{
			fprintf(myDestFile, "error in line %d : multiline comment opened but never closed\n", numberOfCurrentLine);
			return 1;
		}

[^\n\t;]*[*][/]	{
			fprintf(myDestFile, "error in line %d : unopened comment section\n", numberOfCurrentLine);
			return 1;
		}

"//".*	{
			return TOKEN_COMMENT;
}

float	{
			return TOKEN_FLOATTYPE;
		}

char	{
			return TOKEN_CHARTYPE;
		}

string	{
			return TOKEN_STRINGTYPE;
		}

main	{
			return TOKEN_MAINFUNC;
		}

break	{
			return TOKEN_BREAKSTMT;
		}

continue	{
			return TOKEN_CONTINUESTMT;
		}

("-"|"+")?{integer}	{
			char* lastStartChar = yytext;
			int isPositive = 1;
			int error = 0;
			int ommited = 0;
			if(yytext[0] == '-'){
				yytext = &yytext[1];
				ommited = 1;
				isPositive = 0;
				if(strlen(yytext)>strlen("2147483648")){
					error = 1;
				}else if(strlen(yytext)==strlen("2147483648")){
					if(strcmp(yytext, "2147483648")>0){
						error = 1;
					}
				}
			}
			else if(yytext[0] == '+'){
				yytext = &yytext[1];
				ommited = 1;
				if(strlen(yytext)>strlen("2147483647")){
					error = 1;
				}else if(strlen(yytext)==strlen("2147483647")){
					if(strcmp(yytext, "2147483647")>0){
						error = 1;
					}
				}
			} else {
				if(strlen(yytext)>strlen("2147483647")){
					error = 1;
				}else if(strlen(yytext)==strlen("2147483647")){
					if(strcmp(yytext, "2147483647")>0){
						error = 1;
					}
				}
			}
			if(error == 1){
				fprintf(myDestFile, "error in line %d : integer constant is out of range\n", numberOfCurrentLine);
				return 1;
			}else{
				if(ommited == 1) yytext = lastStartChar;
				return TOKEN_INTCONST;
			}
		}

{float}	{
			return TOKEN_FLOATCONST;
		}

["][^["]]*["]	{
			return TOKEN_STRINGCONST;
		}

"'"{letter}"'"	{
			return TOKEN_CHARCONST;
		}

{id}	{
			// if((yytext[0]>='a' && yytext[0]<='z')
			// 	|| (yytext[0]>='A' && yytext[0]<='Z')){
			if(strlen(yytext) > 31) {
				fprintf(myDestFile, "error in line %d : id length can't be more that 31 letters\n", numberOfCurrentLine);
			}
			return TOKEN_ID;
			// 	}
			// else{
			// 	fprintf(myDestFile, "error in line %d : wrong id definition\n", numberOfCurrentLine);
			// }
		}

({digit}|"_")+({id}|"_"|{digit})+ {
				fprintf(myDestFile, "error in line %d : wrong id definition\n", numberOfCurrentLine);
				return 1;
		}

.		{
			printInFile("Didn't Catch", yytext);
		}
%%

void printInFile(char* text) {
    fprintf(myDestFile, "%s", text);
}

void printTokenName(char* tokenName, char* token){
	printInFile(tokenName);
	for(int i = 0; i<30-(int)strlen(tokenName); i++){
		printInFile(" ");
	}
	printInFile(token);
	printInFile("\n");
}