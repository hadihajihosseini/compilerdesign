/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_YY_MYAPP_BISON_HPP_INCLUDED
# define YY_YY_MYAPP_BISON_HPP_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    TOKEN_VOIDTYPE = 258,
    TOKEN_INTTYPE = 259,
    TOKEN_LOOP = 260,
    TOKEN_RETURN = 261,
    TOKEN_IFCONDITION = 262,
    TOKEN_ELSECONDITION = 263,
    TOKEN_PRFUNC = 264,
    TOKEN_DOUBLETYPE = 265,
    TOKEN_FLOATTYPE = 266,
    TOKEN_CHARTYPE = 267,
    TOKEN_STRINGTYPE = 268,
    TOKEN_STRINGCONST = 269,
    TOKEN_MAINFUNC = 270,
    TOKEN_BREAKSTMT = 271,
    TOKEN_CONTINUESTMT = 272,
    TOKEN_ID = 273,
    TOKEN_ARITHMATICOP_SUM = 274,
    TOKEN_ARITHMATICOP_MINUS = 275,
    TOKEN_ARITHMATICOP_MULTIPLY = 276,
    TOKEN_ARITHMATICOP_POWER = 277,
    TOKEN_ARITHMATICOP_DIVIDE = 278,
    TOKEN_LOGICOP_AND = 279,
    TOKEN_LOGICOP_OR = 280,
    TOKEN_LOGICOP_NOT = 281,
    TOKEN_BITWISEOP_BITAND = 282,
    TOKEN_BITWISEOP_BITOR = 283,
    TOKEN_RELATIONOP_LESSTHANOREQUAL = 284,
    TOKEN_RELATIONOP_LESSTHAN = 285,
    TOKEN_RELATIONOP_MORETHAN = 286,
    TOKEN_RELATIONOP_MORETHANOREQUAL = 287,
    TOKEN_RELATIONOP_EQUAL = 288,
    TOKEN_RELATIONOP_NOTEQUAL = 289,
    TOKEN_ASSIGNOP = 290,
    TOKEN_LEFTPAREN = 291,
    TOKEN_RIGHTPAREN = 292,
    TOKEN_LCB = 293,
    TOKEN_RCB = 294,
    TOKEN_SEMICOLON = 295,
    TOKEN_COMMA = 296,
    TOKEN_UNTIL = 297,
    TOKEN_LB = 298,
    TOKEN_RB = 299,
    TOKEN_CHARCONST = 300,
    TOKEN_INTCONST = 301,
    TOKEN_FLOATCONST = 302,
    TOKEN_WHITESPACE = 303
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 9 "syntax.y" /* yacc.c:1909  */

    int intVal;
    float floatVal;
    char charVal;
    char* string;

#line 110 "myapp_bison.hpp" /* yacc.c:1909  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_MYAPP_BISON_HPP_INCLUDED  */
