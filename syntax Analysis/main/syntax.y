%{
    #include <iostream>
    using namespace std;
    extern int yylex();
    extern FILE* yyin;
    int yyerror(const char* error);
%}

%union {
    int intVal;
    float floatVal;
    char charVal;
    char* string;
}

%token <string> TOKEN_VOIDTYPE TOKEN_INTTYPE TOKEN_LOOP TOKEN_RETURN TOKEN_IFCONDITION;
%token <string> TOKEN_ELSECONDITION TOKEN_PRFUNC TOKEN_DOUBLETYPE TOKEN_FLOATTYPE TOKEN_CHARTYPE;
%token <string> TOKEN_STRINGTYPE TOKEN_STRINGCONST TOKEN_MAINFUNC TOKEN_BREAKSTMT TOKEN_CONTINUESTMT;
%token <string> TOKEN_ID;

%token <charVal> TOKEN_ARITHMATICOP_SUM TOKEN_ARITHMATICOP_MINUS TOKEN_ARITHMATICOP_MULTIPLY TOKEN_ARITHMATICOP_POWER TOKEN_ARITHMATICOP_DIVIDE;
%token <charVal> TOKEN_LOGICOP_AND TOKEN_LOGICOP_OR TOKEN_LOGICOP_NOT;
%token <charVal> TOKEN_BITWISEOP_BITAND TOKEN_BITWISEOP_BITOR;
%token <charVal> TOKEN_RELATIONOP_LESSTHANOREQUAL TOKEN_RELATIONOP_LESSTHAN TOKEN_RELATIONOP_MORETHAN TOKEN_RELATIONOP_MORETHANOREQUAL TOKEN_RELATIONOP_EQUAL TOKEN_RELATIONOP_NOTEQUAL;
%token <charVal> TOKEN_ASSIGNOP;
%token <charVal> TOKEN_LEFTPAREN TOKEN_RIGHTPAREN TOKEN_LCB TOKEN_RCB TOKEN_SEMICOLON TOKEN_COMMA;
%token <charVal> TOKEN_UNTIL TOKEN_LB TOKEN_RB TOKEN_CHARCONST;

%token <intVal> TOKEN_INTCONST;

%token <floatVal> TOKEN_FLOATCONST;

%token TOKEN_WHITESPACE;

%right TOKEN_ASSIGNOP
%left TOKEN_LOGICOP_OR
%left TOKEN_LOGICOP_AND
%left TOKEN_BITWISEOP_BITOR
%left TOKEN_ARITHMATICOP_POWER // if it is xor
%left TOKEN_BITWISEOP_BITAND
%left TOKEN_RELATIONOP_EQUAL TOKEN_RELATIONOP_NOTEQUAL
%left TOKEN_RELATIONOP_LESSTHANOREQUAL TOKEN_RELATIONOP_LESSTHAN TOKEN_RELATIONOP_MORETHAN TOKEN_RELATIONOP_MORETHANOREQUAL
%left TOKEN_ARITHMATICOP_SUM TOKEN_ARITHMATICOP_MINUS
%left TOKEN_ARITHMATICOP_MULTIPLY TOKEN_ARITHMATICOP_DIVIDE
%right TOKEN_LOGICOP_NOT

%%

start:              program
                    ;

program:            type main_or_not
                    ;

main_or_not:        main_func
                    | id assign_or_func
                    ;

assign_or_func:     TOKEN_ASSIGNOP exp program
                    | TOKEN_LEFTPAREN f_arg TOKEN_RIGHTPAREN TOKEN_LCB stmts TOKEN_RCB type main_or_func
                    ;

main_or_func:       func
                    | main_func
                    ;

main_func:          TOKEN_MAINFUNC TOKEN_LEFTPAREN f_arg TOKEN_RIGHTPAREN TOKEN_LCB stmts TOKEN_RCB
                    ;

func:               id TOKEN_LEFTPAREN f_arg TOKEN_RIGHTPAREN TOKEN_LCB stmts TOKEN_RCB type main_or_func;

// added by myself
type:               TOKEN_VOIDTYPE
                    | TOKEN_INTTYPE
                    | TOKEN_DOUBLETYPE
                    | TOKEN_FLOATTYPE
                    | TOKEN_CHARTYPE
                    | TOKEN_STRINGTYPE
                    ;

// added by myself
id:                 TOKEN_ID;

f_arg:              type id array_var TOKEN_COMMA f_arg
                    | type id array_var
                    |
                    ;

stmts:              stmt stmts
                    |
                    ;

stmt:               stmt_declare
                    | stmt_assign
                    | stmt_return
                    | loop
                    | condition
                    | call
                    | exp
                    | printfunc
                    | TOKEN_SEMICOLON stmt
                    ;

printfunc:          TOKEN_PRFUNC TOKEN_LEFTPAREN exp TOKEN_RIGHTPAREN stmt
                    ;

exp:                exp TOKEN_ARITHMATICOP_SUM exp
                    | exp TOKEN_ARITHMATICOP_MINUS exp
                    | exp TOKEN_ARITHMATICOP_MULTIPLY exp
                    | exp TOKEN_ARITHMATICOP_POWER exp
                    | exp TOKEN_ARITHMATICOP_DIVIDE exp

                    | exp TOKEN_LOGICOP_AND exp
                    | exp TOKEN_LOGICOP_OR exp

                    | exp TOKEN_BITWISEOP_BITAND exp
                    | exp TOKEN_BITWISEOP_BITOR exp

                    | exp TOKEN_RELATIONOP_LESSTHANOREQUAL exp
                    | exp TOKEN_RELATIONOP_LESSTHAN exp
                    | exp TOKEN_RELATIONOP_MORETHAN exp
                    | exp TOKEN_RELATIONOP_MORETHANOREQUAL exp
                    | exp TOKEN_RELATIONOP_EQUAL exp
                    | exp TOKEN_RELATIONOP_NOTEQUAL exp
                    
                    | TOKEN_ARITHMATICOP_MINUS exp
                    | id TOKEN_LB exp TOKEN_RB
                    | TOKEN_LOGICOP_NOT exp                 // TOKEN_LOGICOP should be = Exclamation Mark
                    | TOKEN_LEFTPAREN exp TOKEN_RIGHTPAREN

                    | id
                    | TOKEN_INTCONST
                    | TOKEN_STRINGCONST
                    | TOKEN_CHARCONST
                    | TOKEN_FLOATCONST
                    ;

loop:               TOKEN_LOOP id TOKEN_LEFTPAREN exp TOKEN_UNTIL exp TOKEN_RIGHTPAREN TOKEN_LCB stmt TOKEN_RCB stmt
                    ;

condition:          TOKEN_IFCONDITION TOKEN_LEFTPAREN exp TOKEN_RIGHTPAREN TOKEN_LCB stmt TOKEN_RCB stmt
                    | elsecon
                    ;

elsecon:            TOKEN_ELSECONDITION TOKEN_LCB stmt TOKEN_RCB stmt
                    ;

stmt_return:        TOKEN_RETURN exp TOKEN_SEMICOLON
                    ;

stmt_declare:       type id array_var ids
                    ;

array_var:          TOKEN_LB exp TOKEN_RB
                    | TOKEN_LB TOKEN_RB
                    |
                    ;

ids:                TOKEN_SEMICOLON stmt
                    | TOKEN_COMMA id array_var ids
                    ;

call:               id TOKEN_LEFTPAREN args TOKEN_RIGHTPAREN stmt
                    ;

args:               id array_var args
                    | TOKEN_COMMA id array_var args
                    |
                    ;

stmt_assign:        id array_var TOKEN_ASSIGNOP exp stmt
                    ;

%%

int main(int argc, char* argv[]) {
    yyin = fopen(argv[1], "r");
    yyparse();
    return 0;
}

int yyerror(const char* error){
    cout << "There is one error: " << error << endl;
    return 0;
}